# Redis for OMniLeads

This repository has the code of Redis & Sentinel component, configuration used for OMniLeads

## Build

To build an image:

```
docker buildx build --file=Dockerfile_redis --tag=$TAG --target=redis .
```

```
docker buildx build --file=Dockerfile_sentinel --tag=$TAG --target=sentinel .
```

Where $TAG is the docker tag you want for image. You can check the version.txt file for the tag.

## Deploy

[OMniLeads Deploy Tool](https://gitlab.com/omnileads/omldeploytool)

## License

GPLV3