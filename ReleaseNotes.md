# Release Notes
2023-11-25

## Added

* oml-414 [BUILD] In the build stage, it's possible to discern between registry containers based on the branch type.

## Changed

## Fixed

## Removed

