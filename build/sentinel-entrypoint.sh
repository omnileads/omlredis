#!/bin/sh

sed -i "s/\$BIND_IP/$BIND_IP/g" /usr/local/etc/redis/sentinel.conf
sed -i "s/redis 6379/$MAIN 6379/g" /usr/local/etc/redis/sentinel.conf
sed -i "s/\$SENTINEL_QUORUM/$SENTINEL_QUORUM/g" /usr/local/etc/redis/sentinel.conf
sed -i "s/\$SENTINEL_DOWN_AFTER/$SENTINEL_DOWN_AFTER/g" /usr/local/etc/redis/sentinel.conf
sed -i "s/\$SENTINEL_FAILOVER/$SENTINEL_FAILOVER/g" /usr/local/etc/redis/sentinel.conf

redis-sentinel /usr/local/etc/redis/sentinel.conf



