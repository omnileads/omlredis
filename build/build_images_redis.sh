#!/bin/bash

if [ $CI_COMMIT_REF_NAME == "master" ]; then
  docker build -f Dockerfile_redis -t omnileads/redis:latest ./
  docker push omnileads/redis:latest
elif [ $CI_COMMIT_REF_NAME == "develop" ]; then
  docker build -f Dockerfile_redis -t omnileads/redis:develop ./
  docker push omnileads/redis:develop
fi

docker build -f Dockerfile_redis -t omnileads/redis:$1 ./
docker push omnileads/redis:$1
