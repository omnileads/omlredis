#!/bin/bash

set -ex
COMMAND="redis-server /usr/local/etc/redis/redis.conf"

sed -i "s/bind 0.0.0.0/bind $BIND_IP/g" /usr/local/etc/redis/redis.conf

echo "***** Cluster ROLE validation: $ROLE *****"
if [[ "$ROLE" == "replica" ]]; then
    echo "***** Cluster ROLE: $ROLE *****"
    echo " " >> /usr/local/etc/redis/redis.conf
    echo "replicaof $MAIN 6379" >> /usr/local/etc/redis/redis.conf
else
    echo "***** MAIN container, cluster ROLE is: $ROLE *****"
    sed -i "s/replica-priority 100/replica-priority 1/g" /usr/local/etc/redis/redis.conf
fi

exec ${COMMAND}

