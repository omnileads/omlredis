#!/bin/bash

if [ $CI_COMMIT_REF_NAME == "master" ]; then
  docker build -f Dockerfile_sentinel -t omnileads/redis_sentinel:latest ../..
  docker push omnileads/redis_sentinel:latest
elif [ $CI_COMMIT_REF_NAME == "develop" ]; then
  docker build -f Dockerfile_sentinel -t omnileads/redis_sentinel:develop ../..
  docker push omnileads/redis_sentinel:develop
fi

docker build -f Dockerfile_sentinel -t omnileads/redis_sentinel:$1 ../..
docker push omnileads/redis_sentinel:$1
